<?php


namespace App\Service;

use Psr\Log\LoggerInterface;

/**
 * Just to create an custom service.
 * @package App\Services
 */
class ExampleService
{

    /**
     * @var LoggerInterface|null
     */
    private $logger;

    /**
     * @var bool
     */
    private $exampleVar;

    /**
     * ExampleService constructor.
     * @param LoggerInterface $logger
     */
    public function __construct(LoggerInterface $logger, bool $exampleVar)
    {
        $this->exampleVar = $exampleVar;
        $this->logger = $logger;
    }

    /**
     * @return string
     */
    public function do_something(string $input): string
    {
        $this->logger->error('Test error message from ExampleService.php');
        $this->logger->error('$exampleVar is: ' . $this->exampleVar);
        return 'We did something with - ' . $input;
    }
}