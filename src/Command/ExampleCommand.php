<?php

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class ExampleCommand extends Command
{
    protected static $defaultName = 'ExampleCommand';

    protected function configure()
    {
        $this
            ->setDescription('This is only an example command')
            ->addArgument('exampleArgument', InputArgument::OPTIONAL, 'This si the description')
            ->addOption('exampleOption', null, InputOption::VALUE_NONE, 'Option description');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $arg1 = $input->getArgument('exampleArgument');

        if ($arg1) {
            $io->note(sprintf('You passed an argument: %s', $arg1));
        }

        $option1 = $input->getOption('exampleOption');
        if ($option1) {
            $io->note(sprintf('You passed an option: %s', $option1));
        }

        $io->success('You have a new command! Now make it your own! Pass --help to see your options.');

        return 0;
    }
}
