<?php

namespace App\Controller;

use App\Service\ExampleService;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


class ArticleController extends AbstractController
{

    /**
     * @Route("/")
     * @return Response
     */
    public function homepage(): Response
    {
        return new Response("Woohoo");
    }

    /**
     * @Route("/news/{slug}")
     * @param string $slug Parameter
     * @return Response
     */
    public function slug($slug): Response
    {
        return new Response("News" . $slug);
    }

    /**
     * @Route("/show", name="app_article_show")
     * @return Response
     */
    public function show(LoggerInterface $logger, ExampleService $example): Response
    {
        $logger->error('Something BAAAADDDD happened...');

        $res = $example->do_something('JAP');

        $comments = [
            'I ate a normal rock once. It did NOT taste like bacon!',
            'Woohoo! I\'m going on an all-asteroid diet!',
            'I like bacon too! Buy some from my site! bakinsomebacon.com',
        ];

        return $this->render('article/show.html.twig', [
            'title' => $res,
            'comments' => $comments,
        ]);
    }
}
